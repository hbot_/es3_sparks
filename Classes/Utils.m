/*
 *  Utils.c
 *  Lab3
 *
 *  Created by John Williamson on 09/02/2010.
 *  Copyright 2010 Computing science. All rights reserved.
 *
 */

#include "Utils.h"

GLuint  loadTexture(NSString *name, int *w, int *h)
{
	NSString *path = [[NSBundle mainBundle] pathForResource:name ofType:@"png"];
	NSData *texData = [[NSData alloc] initWithContentsOfFile:path];
	UIImage *image = [[UIImage alloc] initWithData:texData];
	if (image == nil)
		[NSException raise:@"Texture loading error!" format:@"Texture loading error!"];
	GLuint textureName;
	*w = CGImageGetWidth(image.CGImage);
	*h = CGImageGetHeight(image.CGImage);
	glGenTextures(1, &textureName);
	
	glBindTexture(GL_TEXTURE_2D, textureName);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	void *imageData = malloc( *h * *w * 4 );
	CGContextRef context = CGBitmapContextCreate( imageData, *w, *h, 8, 4 * *w, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big );
	CGColorSpaceRelease( colorSpace );
	CGContextClearRect( context, CGRectMake( 0, 0, *w, *h ) );
	CGContextTranslateCTM( context, 0, *h - *h );
	CGContextDrawImage( context, CGRectMake( 0, 0, *w, *h ), image.CGImage );
	
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, *w, *h, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData);
	
	CGContextRelease(context);
	
	free(imageData);
	[image release];
	[texData release];
	return textureName;
	
}

double generateGaussian()
{
	double sum = 0.0;
	for(int i=0;i<12;i++)
	{
		double v = arc4random()/((float)0xffffffff);
		sum += v-0.5;
	}
	return sum;
}

