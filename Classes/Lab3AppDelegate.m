//
//  Lab3AppDelegate.m
//  Lab3
//
//  Created by John Williamson on 05/02/2010.
//  Copyright Computing science 2010. All rights reserved.
//

#import "Lab3AppDelegate.h"
#import "EAGLView.h"

@implementation Lab3AppDelegate

@synthesize window;
@synthesize glView;

- (void) applicationDidFinishLaunching:(UIApplication *)application
{
	[glView startAnimation];
}

- (void) applicationWillResignActive:(UIApplication *)application
{
	[glView stopAnimation];
}

- (void) applicationDidBecomeActive:(UIApplication *)application
{
	[glView startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	[glView stopAnimation];
}

- (void) dealloc
{
	[window release];
	[glView release];
	
	[super dealloc];
}

@end
