//
//  EAGLView.m
//  Lab3
//
//  Created by John Williamson on 05/02/2010.
//  Copyright Computing science 2010. All rights reserved.
//

#import "EAGLView.h"
#import "ES1Renderer.h"
#import "Particle.h"
#import "Utils.h"
//#import "AVFoundation/AVFoundation.h"

@implementation EAGLView

@synthesize animating;
@synthesize particleArray;
@synthesize current_x, current_y, isPressed;
@synthesize second_x, second_y, secondIsPressed;
@synthesize audioPlayer;
@dynamic animationFrameInterval;




// You must implement this method
+ (Class) layerClass
{
    return [CAEAGLLayer class];
}




//The GL view is stored in the nib file. When it's unarchived it's sent -initWithCoder:
- (id) initWithCoder:(NSCoder*)coder
{    
    if ((self = [super initWithCoder:coder]))
	{
        // Get the layer
        CAEAGLLayer *eaglLayer = (CAEAGLLayer *)self.layer;
        
        eaglLayer.opaque = TRUE;
        eaglLayer.drawableProperties = [NSDictionary dictionaryWithObjectsAndKeys:
                                        [NSNumber numberWithBool:FALSE], kEAGLDrawablePropertyRetainedBacking, kEAGLColorFormatRGBA8, kEAGLDrawablePropertyColorFormat, nil];
		
		renderer = [[ES1Renderer alloc] init];
		
		animating = FALSE;
		displayLinkSupported = FALSE;
		animationFrameInterval = 1;
		displayLink = nil;
		animationTimer = nil;
		
		
		// A system version of 3.1 or greater is required to use CADisplayLink. The NSTimer
		// class is used as fallback when it isn't available.
		NSString *reqSysVer = @"3.1";
		NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
		if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending)
			displayLinkSupported = TRUE;
        
        particleArray = [[NSMutableArray alloc] initWithCapacity:500];
        [(ES1Renderer *)renderer setParticles:particleArray ];
        
        isPressed = NO;
        secondIsPressed = NO;
        
        second_x = 0;
        second_y = 0;
        
        NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/sparking.wav", [[NSBundle mainBundle] resourcePath]]];
        
        NSError *error;
        audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
        audioPlayer.numberOfLoops = -1;
        
        dx = 0;
        dy = -1;
        
    }
	
    return self;
}

- (void) updateParticles {
    
    if (isPressed) {
        int r = arc4random() % 5;
        for (int i = 0; i < r; i++){
            Particle *p = [[Particle alloc] init];
            p.x = current_x;
            p.y = 480 - current_y;
            int constant = -2;
            if (!secondIsPressed){
                p.dx = generateGaussian() + constant;
                p.dy = generateGaussian() + constant;
            }
            else{
                p.dx = (second_x - current_x) / 5;
                p.dy = (current_y - second_y) / 5;
            }
            [particleArray addObject:p];
        }
    }
    
       
    
    
    NSMutableArray *killList = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < particleArray.count; i++){
        Particle *p = [particleArray objectAtIndex:i];
        
        if(p.lifetime <= 0){
            [killList addObject:p];
            continue;
        }
        [p update];
    }
    
    for (int i = 0; i < killList.count; i++){
        Particle *p = [killList objectAtIndex:i];
        [particleArray removeObject:p];
        [p release];
    }
}

- (void) drawView:(id)sender
{
    [renderer render];
    [self updateParticles];
}

/**** touches methods ****/

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    //[super touchesBegan:touches withEvent:event];
    isPressed = YES;
    
    if(audioPlayer == nil)
        NSLog(@"Something wrong with audioplayer");
    else
        [audioPlayer play];
    
    NSSet *allTouches = [event allTouches];
    
    if (allTouches.count == 1) {
        secondIsPressed = NO;
        UITouch *touch = [touches anyObject];
        CGPoint touchCoordinates = [touch locationInView:self];
        current_x = touchCoordinates.x;
        current_y = touchCoordinates.y;
    }
    else 
    {
        UITouch *firstTouch = [[allTouches allObjects] objectAtIndex:0];
        UITouch *secondTouch = [[allTouches allObjects] objectAtIndex:1];
        
        CGPoint firstTouchCoordinates = [firstTouch locationInView:self];
        CGPoint secondTouchCoordinates = [secondTouch locationInView:self];
        
        current_x = firstTouchCoordinates.x;
        current_y = firstTouchCoordinates.y;
        
        secondIsPressed = YES;
        
        second_x = secondTouchCoordinates.x;
        second_y = secondTouchCoordinates.y;
    }
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    
    isPressed = NO;
    secondIsPressed = NO;
    second_x = 0;
    second_y = 0;
    
    dx = 0;
    dy = -1;
    
    if(audioPlayer == nil)
        NSLog(@"Something wrong with audioplayer");
    else
        [audioPlayer pause];
        
}

- (void) touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
    
    NSSet *allTouches = [event allTouches];
    
    if (allTouches.count == 1) {
        secondIsPressed = NO;
        UITouch *touch = [touches anyObject];
        CGPoint touchCoordinates = [touch locationInView:self];
        current_x = touchCoordinates.x;
        current_y = touchCoordinates.y;
    }
    else 
    {
        secondIsPressed = YES;
        UITouch *firstTouch = [[allTouches allObjects] objectAtIndex:0];
        UITouch *secondTouch = [[allTouches allObjects] objectAtIndex:1];
        
        CGPoint firstTouchCoordinates = [firstTouch locationInView:self];
        CGPoint secondTouchCoordinates = [secondTouch locationInView:self];
        
        current_x = firstTouchCoordinates.x;
        current_y = firstTouchCoordinates.y;
        
        second_x = secondTouchCoordinates.x;
        second_y = secondTouchCoordinates.y;
    }
}

/************************/

- (void) layoutSubviews
{
	[renderer resizeFromLayer:(CAEAGLLayer*)self.layer];
    [self drawView:nil];
}

- (NSInteger) animationFrameInterval
{
	return animationFrameInterval;
}

- (void) setAnimationFrameInterval:(NSInteger)frameInterval
{
	// Frame interval defines how many display frames must pass between each time the
	// display link fires. The display link will only fire 30 times a second when the
	// frame internal is two on a display that refreshes 60 times a second. The default
	// frame interval setting of one will fire 60 times a second when the display refreshes
	// at 60 times a second. A frame interval setting of less than one results in undefined
	// behavior.
	if (frameInterval >= 1)
	{
		animationFrameInterval = frameInterval;
		
		if (animating)
		{
			[self stopAnimation];
			[self startAnimation];
		}
	}
}

- (void) startAnimation
{
	if (!animating)
	{
		if (displayLinkSupported)
		{
			// CADisplayLink is API new to iPhone SDK 3.1. Compiling against earlier versions will result in a warning, but can be dismissed
			// if the system version runtime check for CADisplayLink exists in -initWithCoder:. The runtime check ensures this code will
			// not be called in system versions earlier than 3.1.

			displayLink = [NSClassFromString(@"CADisplayLink") displayLinkWithTarget:self selector:@selector(drawView:)];
			[displayLink setFrameInterval:animationFrameInterval];
			[displayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
		}
		else
			animationTimer = [NSTimer scheduledTimerWithTimeInterval:(NSTimeInterval)((1.0 / 60.0) * animationFrameInterval) target:self selector:@selector(drawView:) userInfo:nil repeats:TRUE];
		
		animating = TRUE;
	}
}

- (void)stopAnimation
{
	if (animating)
	{
		if (displayLinkSupported)
		{
			[displayLink invalidate];
			displayLink = nil;
		}
		else
		{
			[animationTimer invalidate];
			animationTimer = nil;
		}
		
		animating = FALSE;
	}
}


- (void) dealloc
{
    [renderer release];
	
    [super dealloc];
}

@end
