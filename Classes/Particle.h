//
//  Particle.h
//  Lab3
//
//  Created by Horatio Bota on 14/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Particle : NSObject

{
    
    GLfloat x;
    GLfloat y;
    GLfloat dx;
    GLfloat dy;
    GLfloat maxLife;
    GLfloat lifetime;
    GLfloat angle;
    GLfloat scale;
    
}

@property (nonatomic, assign) GLfloat x;
@property (nonatomic, assign) GLfloat y;
@property (nonatomic, assign) GLfloat dx;
@property (nonatomic, assign) GLfloat dy;
@property (nonatomic, assign) GLfloat maxLife;
@property (nonatomic, assign) GLfloat lifetime;
@property (nonatomic, assign) GLfloat angle; 
@property (nonatomic, assign) GLfloat scale;

- (id) init;
- (void) update;

@end
