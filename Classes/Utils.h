/*
 *  Utils.h
 *  Lab3
 *
 *  Created by John Williamson on 09/02/2010.
 *  Copyright 2010 Computing science. All rights reserved.
 *
 */

#import <Foundation/Foundation.h>
#import <OpenGLES/ES1/gl.h>
#import <OpenGLES/ES1/glext.h>

GLuint  loadTexture(NSString *name, int *w, int *h);
double generateGaussian();
