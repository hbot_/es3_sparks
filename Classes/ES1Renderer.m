//
//  ES1Renderer.m
//  Lab3
//
//  Created by John Williamson on 05/02/2010.
//  Copyright Computing science 2010. All rights reserved.
//

#import "ES1Renderer.h"
#import "Utils.h"
#import "Particle.h"

@implementation ES1Renderer
@synthesize particles;

// Create an ES 1.1 context
- (id) init
{
	if (self = [super init])
	{
		context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES1];
        
        if (!context || ![EAGLContext setCurrentContext:context])
		{
            [self release];
            return nil;
        }
		
		// Create default framebuffer object. The backing will be allocated for the current layer in -resizeFromLayer
		glGenFramebuffersOES(1, &defaultFramebuffer);
		glGenRenderbuffersOES(1, &colorRenderbuffer);
		glBindFramebufferOES(GL_FRAMEBUFFER_OES, defaultFramebuffer);
		glBindRenderbufferOES(GL_RENDERBUFFER_OES, colorRenderbuffer);
		glFramebufferRenderbufferOES(GL_FRAMEBUFFER_OES, GL_COLOR_ATTACHMENT0_OES, GL_RENDERBUFFER_OES, colorRenderbuffer);
		
        int w,h;
        background_texture = loadTexture(@"background", &w, &h);
        
        //spark image
        spark_texture = loadTexture(@"spark", &w, &h);
        
        //
        particles = [[NSMutableArray alloc] init];
        
        
	}
	
	return self;
}

- (void) drawParticle:(Particle *)p {
    
    glEnable(GL_TEXTURE_2D);    //Enable texturing
    glEnableClientState(GL_VERTEX_ARRAY); //Enable the right arrays
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    GLfloat texCoords[] = {0,1, 1,1, 0,0, 1,0};
    GLfloat vertices[] = {0,1,0, 1,1,0, 0,0,0, 1,0,0};

    glPushMatrix();         // store modelview matrix
    
    glTranslatef(p.x, p.y, 0);
    
    glRotatef(p.angle * 57.2957759, 0, 0, 1);
    
    glScalef(32 * p.scale, 32 * p.scale, 1);// map (0,0), (1,1) to (0,0), (32,32)
    glColor4f(1,1,1,p.lifetime/p.maxLife);
    glBindTexture(GL_TEXTURE_2D, spark_texture); //set the current texture
    glVertexPointer(3, GL_FLOAT, 0, vertices);        //set the pointers
    glTexCoordPointer(2, GL_FLOAT, 0, texCoords);     
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glPopMatrix();              //restore modelview matrix
    glDisable(GL_TEXTURE_2D);   //IMPORTANT:disable texturing again
    
}

- (void) drawBackground {
    
    glEnable(GL_TEXTURE_2D);    //Enable texturing
    glEnableClientState(GL_VERTEX_ARRAY); //Enable the right arrays
    glDisableClientState(GL_NORMAL_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    GLfloat texCoords[] = {0,480/512.0, 320/512.0,480/512.0, 0,0, 320/512.0,0};
    GLfloat vertices[] = {0,1,0, 1,1,0, 0,0,0, 1,0,0};
    
    glPushMatrix();         // store modelview matrix
    
    glScalef(320, 480, 1);   // map (0,0), (1,1) to (0,0), (320,480)
    glColor4f(1,1,1,1);     // white color
    glBindTexture(GL_TEXTURE_2D, background_texture); //set the current texture
    glVertexPointer(3, GL_FLOAT, 0, vertices);        //set the pointers
    glTexCoordPointer(2, GL_FLOAT, 0, texCoords);     
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    glPopMatrix();              //restore modelview matrix
    glDisable(GL_TEXTURE_2D);   //IMPORTANT:disable texturing again
    
}

- (void) render
{
    // Replace the implementation of this method to do your own custom drawing
    
	[EAGLContext setCurrentContext:context];
    
	// This application only creates a single default framebuffer which is already bound at this point.
	// This call is redundant, but needed if dealing with multiple framebuffers.
    glBindFramebufferOES(GL_FRAMEBUFFER_OES, defaultFramebuffer);
    //glViewport(0, 0, backingWidth, backingHeight);
	
    // Rendering stuff goes here!!!
    
    
    //glClearColor(1, 0.65, 0.65, 1);
    glViewport(0, 0, backingWidth, backingHeight); // specify the screen size
    glClear(GL_COLOR_BUFFER_BIT);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    glOrthof(0, backingWidth, 0, backingHeight, -1, 1);
    
    [self drawBackground];
    
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    
    for (int i = 0; i < particles.count; i++){
        Particle *p = [particles objectAtIndex:i];
        [self drawParticle:p];
    }

    
    
    //going back to modelview
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    // Rendering stuff goes here!!!
    
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, colorRenderbuffer);
    [context presentRenderbuffer:GL_RENDERBUFFER_OES];
}

- (BOOL) resizeFromLayer:(CAEAGLLayer *)layer
{	
	// Allocate color buffer backing based on the current layer size
    glBindRenderbufferOES(GL_RENDERBUFFER_OES, colorRenderbuffer);
    [context renderbufferStorage:GL_RENDERBUFFER_OES fromDrawable:layer];
	glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_WIDTH_OES, &backingWidth);
    glGetRenderbufferParameterivOES(GL_RENDERBUFFER_OES, GL_RENDERBUFFER_HEIGHT_OES, &backingHeight);
	
    if (glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES) != GL_FRAMEBUFFER_COMPLETE_OES)
	{
		NSLog(@"Failed to make complete framebuffer object %x", glCheckFramebufferStatusOES(GL_FRAMEBUFFER_OES));
        return NO;
    }
    
    return YES;
}

- (void) dealloc
{
	// Tear down GL
	if (defaultFramebuffer)
	{
		glDeleteFramebuffersOES(1, &defaultFramebuffer);
		defaultFramebuffer = 0;
	}

	if (colorRenderbuffer)
	{
		glDeleteRenderbuffersOES(1, &colorRenderbuffer);
		colorRenderbuffer = 0;
	}
	
	// Tear down context
	if ([EAGLContext currentContext] == context)
        [EAGLContext setCurrentContext:nil];
	
	[context release];
	context = nil;
	
	[super dealloc];
}

@end
