//
//  Particle.m
//  Lab3
//
//  Created by Horatio Bota on 14/02/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Particle.h"

@implementation Particle

@synthesize x, y, dx, dy, maxLife, lifetime, angle, scale;

- (id) init {
    
    scale = arc4random()/(double)(0xfffffff)/10;
    maxLife = 500;
    lifetime = maxLife;
    NSLog(@"%f\n", scale);
    return self;
}

- (void) update {

    GLfloat air_resistance = 0.8;

    if ( y <= 1 ) {
        y = 0;
        dy = 0;
    }
    else
    {
        y += dy;
        //dx += 0;
        dy += -1;
        dy *= air_resistance;
    }
    
    if ( x <= 0 ) {
        
        x = 0;
        dx = 0;
        
    }
    else {
    x += dx;
    }
    //y += dy;
    
    //lifetime
    lifetime--;
    
    //rotation
    angle = atan2(dy, dx);
    
    //air_resistance
    dx *= air_resistance;
    //dy *= air_resistance;

        
}

@end
